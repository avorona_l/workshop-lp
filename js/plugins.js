// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

//  jQuery toggle

$(document).ready(function() {
    $('.plus-toggle').click(function() {
        //get collapse content selector
        var collapse_content_selector = $(this).attr('data-target');

        //make the collapse content to be shown or hide
        var toggle_switch = $(this);
        $(collapse_content_selector).slideToggle(400, function() {});
    });
});


//jQuery scrolling on map asctivating (for lg screens only)

function scrolling() {
    if (window.innerWidth > 992) {
        $('.gmap').on('click touchstart', function() {
            $('.gmap iframe').css("pointer-events", "auto");
        });

        $(".gmap").mouseleave(function() {
            $('.gmap iframe').css("pointer-events", "none");
        });
    } else {
        console.log("TEST");
    }
};
scrolling();
