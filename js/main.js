

// ==== bxSldier

$(document).ready(function(){
  $('.sertificate-slider').bxSlider( {
  	prevSelector: $(".slide-prev"),
  	nextSelector: $(".slide-next"),
  	nextText: "e",
  	prevText: "e",
  	controls:true,
  	maxSlides: 1,
  	pager:false
  });
});

// ==== COUNTODOWN TIMER

$(function() {
 
var end = new Date('05/31/2017 04:0 PM'); 
var _milisec = 10;
var _second = _milisec * 100;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour * 24;
function showRemaining() {
var now = new Date();
var distance = end - now;
if (distance < 0) {
$('.t-days').text("00");
$('.t-hours').text("00");
$('.t-mins').text("00");
$('.t-sec').text("00");
$('.t-milisec').text("00");
return;
}
var days = Math.floor(distance / _day);
var hours = Math.floor((distance % _day) / _hour);
var minutes = Math.floor((distance % _hour) / _minute);
var seconds = Math.floor((distance % _minute) / _second);
var miliseconds = Math.floor((distance % _second) / _milisec);
if (seconds < 10) seconds = '0' + seconds;
if (minutes < 10) minutes = '0' + minutes;
if (hours < 10) hours = '0' + hours;
$('.t-days').text(days);
$('.t-hours').text(hours);
$('.t-mins').text(minutes);
$('.t-sec').text(seconds);
$('.t-milisec').text(miliseconds);
}
setInterval(showRemaining, 10);
});

// ==== MODAL WINDOW

// 1.Vanilla JavaScript solution

// Thanks to Paul_Wikins for solution of vanilla js modal window which can run 
// have IE support.
 /*
  https://www.sitepoint.com/community/
  t/how-to-get-this-script-to-work-for-multiple-modal-popups/252443/8  

  */

/*global window*/
(function iife() {
    "use strict";
    function closestEl(el, selector) {
        var doc = el.document || el.ownerDocument;
        var matches = doc.querySelectorAll(selector);
        var i;
        while (el) {
            i = matches.length - 1;
            while (i >= 0) {
                if (matches.item(i) === el) {
                    return el;
                }
                i -= 1;
            }
            el = el.parentElement;
        }
        return el;
    }
    var modalBtns = document.querySelectorAll(".modal-toggle");
    modalBtns.forEach(function addBtnClickEvent(btn) {
        btn.onclick = function showModal() {
            var modal = btn.getAttribute("data-modal");
            document.getElementById(modal).style.display = "block";
        };
    });

    var closeBtns = document.querySelectorAll(".close");
    closeBtns.forEach(function addCloseClickEvent(btn) {
        btn.onclick = function closeModal() {
            var modal = closestEl(btn, ".modal");
            modal.style.display = "none";
        };
    });

    window.onclick = function closeOnClick(event) {
        if (event.target.className === "modal") {
            event.target.style.display = "none";
        }
    };
}());

// 2.jQuery solution
// $(".modal-toggle").on("click", function() {
//   var modal = $(this).data("modal");
//   $(modal).show();
// });

// $(".modal").on("click", function(e) {
//   var className = e.target.className;
//   if(className === "modal" || className === "close"){
//     $(this).closest(".modal").hide();
//   }
// });